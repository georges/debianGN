#!/bin/bash

# Mettre à jour la liste des paquets
apt-get update

# Créer l'utilisateur "visiteur" avec le mot de passe "visiteur" s'il n'existe pas déjà
if ! id "visiteur" &>/dev/null; then
    useradd -m -s /bin/bash visiteur
    echo "visiteur:visiteur" | chpasswd
    usermod -aG sudo visiteur
    echo "L'utilisateur 'visiteur' a été créé avec succès."
else
    echo "L'utilisateur 'visiteur' existe déjà."
fi

# Installer les paquets nécessaires
apt-get install -y wget dpkg

# Télécharger et installer Tabby
TABBY_URL="https://github.com/Eugeny/tabby/releases/download/v1.0.207/tabby-1.0.207-linux-x64.deb"
TABBY_DEB="/tmp/tabby.deb"
wget -O $TABBY_DEB $TABBY_URL
dpkg -i $TABBY_DEB || apt-get -f install -y

# Télécharger et installer VSCodium
CODIUM_URL="https://github.com/VSCodium/vscodium/releases/download/1.89.1.24130/codium_1.89.1.24130_amd64.deb"
CODIUM_DEB="/tmp/codium.deb"
wget -O $CODIUM_DEB $CODIUM_URL
dpkg -i $CODIUM_DEB || apt-get -f install -y

# Résoudre les dépendances manquantes
apt-get -f install -y

# Copier le script post-installation dans le système cible
# Assurez-vous que le chemin est correct
SCRIPT_PATH="/home/visiteur/post-install.sh"
cp /cdrom/custom-scripts/post-install.sh /target$SCRIPT_PATH
chown visiteur:visiteur /target$SCRIPT_PATH
chmod +x /target$SCRIPT_PATH

# Message de bienvenue
echo "Bienvenue sur votre nouveau système Debian!" > /home/visiteur/welcome.txt
chown visiteur:visiteur /home/visiteur/welcome.txt

# Message de fin
echo "Script post-install.sh terminé."
